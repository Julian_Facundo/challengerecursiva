#include "inscripciones.h"

inscripciones::inscripciones(){ //constructor inicializo todo vacio
	inscripciones::nombre = {};
	inscripciones::edad= {};
	inscripciones::equipos = {};
	inscripciones::estadoCivil = {};
	inscripciones::nivelEs = {};
	inscripciones::numDeentradas = 0;
	inscripciones::equiposDif = {};
}

void inscripciones::agregar_entrada(string linea) { //leo la linea de getline()
	if (linea[0] == '\n') {
		return;
	}
	int i = 0;
	inscripciones::nombre.push_back(""); //coloco una entrada vacia en todos
	inscripciones::edad.push_back("");
	inscripciones::equipos.push_back("");
	inscripciones::estadoCivil.push_back(true);
	inscripciones::nivelEs.push_back("");

	int puntoc = 0; //cuenta las divisiones por ;
	while (linea[i] != '\n') {
		if (linea[i] != ';') {
			if (puntoc == 0) {
				nombre[numDeentradas] = nombre[numDeentradas] + linea[i]; 
			}
			else if (puntoc == 1) {
				edad[numDeentradas] = edad[numDeentradas] + linea[i];
			}
			else if (puntoc == 2) {
				equipos[numDeentradas] = equipos[numDeentradas] + linea[i];
			}
			else if (puntoc == 3) {
				if (linea[i] == 'C') {
					estadoCivil[numDeentradas] = true;
					i = i + 6;
				} 
				else {
				    estadoCivil[numDeentradas] = false;
					i = i + 7;
				}
				puntoc++;
			}
		    else if (puntoc == 4) {
				nivelEs[numDeentradas] = nivelEs[numDeentradas] + linea[i];
			}
		} else {
			puntoc++;
		}
		i++;
	}
	//agrego la lista de equipos con su # de integrantes
	i = 0;
	bool Noesta = true;
	string equipo = equipos[numDeentradas];
	int k = equiposDif.size();
	while (i<k && Noesta) {
		if (equiposDif[i].first == equipo) {
			equiposDif[i].second++;
			Noesta = false;
		}
		i++;
	}
	if (Noesta) {
		pair<string, int> nuevo;
		nuevo.first = equipo;
		nuevo.second = 1;
		equiposDif.push_back(nuevo);
	}
	inscripciones::numDeentradas++;
}

int inscripciones::Promedio_de_edades(string equipo) {
	int prom = 0; //el promedio debe terminar aca
	int numeq = 0; //cuenta los miembros del equipo
	int k = 0; 
	int i = 0;
	while (i <= numDeentradas-1) {
		if (equipo == inscripciones::equipos[i]) {
			k = stoi(edad[i]);
			prom += k;
			numeq++;
		}
		i++;
	}

	if (prom == 0) { //si meti un equipo que no esta en la lista devuelve
		return 0;
	}
	return prom / numeq; //devuelvo la suma de edades dividido el num de miembros esto me va a redondear siempre para abajo
}

int  inscripciones::edad_minima(string equipo) {
	int min = 1000;
	int i = 0;
	while (i < edad.size()) {
		if (equipo == equipos[i] && stoi(edad[i]) < min) {
			min = stoi(edad[i]);
		}
		i++;
	}

	return min;
}

int  inscripciones::edad_maxima(string equipo) {
	int max = 0;
	int i = 0;
	while (i < edad.size()) {
		if (equipo == equipos[i] && stoi(edad[i]) > max) {
			max = stoi(edad[i]);
		}
		i++;
	}

	return max;
}


vector<pair<string, int>> nombres_y_cuanto_se_repiten (string equipo, inscripciones registro) { //vector que contiene todos los nombres en un equipo y cuantas veces aparecen
	vector<pair<string, int>> nombresSinRep = {};
	int i = 0;
	int k = registro.nombre.size();
	while (i < k) {
		if (registro.equipos[i] == equipo) {
			bool Noesta = true;
			int j = 0;
			int g = nombresSinRep.size();
			string nombreBuscado = registro.nombre[i];
				while(j<g){
					if (nombresSinRep[j].first == nombreBuscado) {
						nombresSinRep[j].second++;
						Noesta = false;
					}
					j++;
				}
				if (Noesta) {
					pair<string, int> nuevo;
					nuevo.first = nombreBuscado;
					nuevo.second = 1;
					nombresSinRep.push_back(nuevo);
				}
		}
		i++;
	}
	
	return nombresSinRep;
}

void printNombresMasComunes(int x, string equipo, inscripciones registro) { //ordena los nombres de Mayor a menor por repeticiones e imprime las primeras x 
	vector<pair<string, int>> nombresSinRep = nombres_y_cuanto_se_repiten(equipo, registro);

	nombresSinRep = mergesortMam(nombresSinRep);
	int i = 0;
	int g = nombresSinRep.size();
	while (i<x && i< g) { //si x fuera a ser mas grande que el tama�o, imprimo lo que puedo
		cout << nombresSinRep[i].first << '\n';
		i++;
	}

}

vector<pair<string, int>> mergesortmaM(vector<pair<string, int>> entrada) { //ordena menor a mayor 
		if (entrada.size() > 1) {
			int mid = entrada.size() / 2;
			vector<pair<string, int>> izq(entrada.begin(), entrada.begin() + mid); //divido el vector en 2
			vector<pair<string, int>> der(entrada.begin() + mid, entrada.begin() + entrada.size());
			izq = mergesortmaM(izq); //izq y der estan ordenados
			der = mergesortmaM(der);

			int i = 0; 
			int j = 0;
			int g = 0;
			while (i < izq.size() && j < der.size()) { //uno los ordenados
				if (izq[i].second < der[j].second) {
					entrada[g] = izq[i];
					i++;
				} else {
					entrada[g] = der[j];
					j++;
				}
				g++;

				}

			while (i < izq.size()) { //si falto de izq los meto al final
				entrada[g] = izq[i];
				i++;
				g++;
			}

			while (j < der.size()) { //si falto de der los meto al final
				entrada[g] = der[j];
				j++;
				g++;
			}
		}
	
		return entrada;
}

vector<pair<string, int>> mergesortMam(vector<pair<string, int>> entrada) { //ordena Mayor a menor 
	if (entrada.size() > 1) {
		int mid = entrada.size() / 2;
		vector<pair<string, int>> izq(entrada.begin(), entrada.begin() + mid); //divido el vector en 2
		vector<pair<string, int>> der(entrada.begin() + mid, entrada.begin() + entrada.size());
		izq = mergesortMam(izq); //izq y der estan ordenados
		der = mergesortMam(der);

		int i = 0;
		int j = 0;
		int g = 0;
		while (i < izq.size() && j < der.size()) { //uno los ordenados
			if (izq[i].second > der[j].second) {
				entrada[g] = izq[i];
				i++;
			}
			else {
				entrada[g] = der[j];
				j++;
			}
			g++;

		}

		while (i < izq.size()) { //si falto de izq los meto al final
			entrada[g] = izq[i];
			i++;
			g++;
		}

		while (j < der.size()) { //si falto de der los meto al final
			entrada[g] = der[j];
			j++;
			g++;
		}
	}

	return entrada;
}


void ListadoDeSociosYPromDeEdad(inscripciones registro) { //ordena los equipos, los ordena de mayor a menor por cuanto socios tienen y los imprime
	vector<pair<string, int>> equipos = registro.equiposDif;
	equipos = mergesortMam(equipos);
	int i = 0;
	int g = 0;
	int j = 0; 

	cout << "equipo              |" << "promedio de edades |" << "edad maxima |" << "edad minima" << '\n';
	cout<< '\n';
	while (i < equipos.size()) {
		g = 20 - equipos[i].first.size();
		string recuadro = "";

		while (g > 0) {
			recuadro = recuadro + ' ';
			g--;
		}
		cout << equipos[i].first<<recuadro+'|';
		recuadro = "                 |";
		cout << registro.Promedio_de_edades(equipos[i].first) << recuadro;

		cout << registro.edad_maxima(equipos[i].first) << recuadro;

		cout << registro.edad_minima(equipos[i].first) << '\n';

		i++;
	}
	cout << '\n';
}



void personas_con_caracteristicas(inscripciones entrada, int x, string estudios, bool estado) {
	int i = 0;
	int m = 0;
	vector<pair<string, int>> listado = {};
	while (m < x && i < entrada.numDeentradas) {
		if (entrada.nivelEs[i] == "Universitario" && entrada.estadoCivil[i]) {
			pair<string, int> valor;
			valor.first= entrada.nombre[i] + ";" + entrada.equipos[i];
			valor.second = stoi(entrada.edad[i]);
			listado.push_back(valor);
			m++;
		}
		i++;
	}
	listado = mergesortmaM(listado);
	i = 0;
	m = 0;
	int Tamlistado = listado.size();
	cout << "nombre   |" << "edad |" << "equipo"<< '\n';
	while (i < x && i < Tamlistado) {
		m = 0;
		string nombre = "";

		while (listado[i].first[m] != ';') {
			nombre = nombre + listado[i].first[m];
			m++;
		}
		m++;
		string equipo  (listado[i].first.begin() + m, listado[i].first.begin() + listado[i].first.size());
		int g = 9 - nombre.size();
		string recuadro = "";

		while (g > 0) {
			recuadro = recuadro + ' ';
			g--;
		}

		cout << i<<". " << nombre + recuadro + '|' << listado[i].second <<"   |"<<equipo << '\n';
		i++;
	}

}

