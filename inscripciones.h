#pragma once
#ifndef INSCRIPCION
#define INSCRIPCION
#include <istream>
#include <fstream>
#include <wchar.h> 
#include <locale.h>
#include <vector>
#include <string>
#include <iostream>
using namespace std;


class inscripciones
{
public:
	//Data member
	vector<string> nombre;
	vector<string> edad;
	vector<string> equipos;
	vector<bool> estadoCivil; //true si casado, false si soltero
	vector<string> nivelEs;
	int numDeentradas;
	vector<pair<string,int>> equiposDif; //listado con los nombres de cada equipo sin repetir y la cantidad de miembros


	//Member functions
	inscripciones(); //constructor
	void agregar_entrada(string linea);
	int edad_minima(string equipo);
	int edad_maxima(string equipo);
	int Promedio_de_edades(string equipo); //devuelve un entero con el promedio de edades de un equipo redondeado para la parte entera
};

//funciones utiles
void ListadoDeSociosYPromDeEdad(inscripciones registro);
void printNombresMasComunes(int x, string equipo, inscripciones registro);
vector<pair<string, int>> nombres_y_cuanto_se_repiten(string equipo, inscripciones registro);
vector<pair<string, int>> mergesortmaM(vector<pair<string, int>> entrada);
vector<pair<string, int>> mergesortMam(vector<pair<string, int>> entrada);
void personas_con_caracteristicas(inscripciones entrada, int x, string estudios, bool estado); //imprime por consola un listado con las primeras x personas con el estudio y estado civil seteado

#endif