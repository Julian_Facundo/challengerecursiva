#include "inscripciones.h"

int main()
{
    ifstream socio;
    socio.open("socios.csv");
    std::string name;

    inscripciones inscriptos= inscripciones();
    while (!socio.eof()){
        getline(socio, name);
        inscriptos.agregar_entrada(name + '\n');
    }


    //string registrados = to_string(a.numDeentradas);
    setlocale(LC_ALL, ""); //agrego esta funcion para poder imprimir acentos
    cout << "1) cantidad de registrados: "<< inscriptos.numDeentradas << '\n';
    cout << '\n';
    cout << "2) promedio de edades de los socios de Racing: " << inscriptos.Promedio_de_edades("Racing") << '\n';
    cout << '\n';
    cout << "3) Listado de los primeros 100 inscriptos casados y con nivel universitario por nombres, edades y  " << '\n';
    cout << '\n';
    personas_con_caracteristicas(inscriptos, 100, "Universitario", true);
    cout << '\n';
    cout << "4) listado con los 5 nombres m�s comunes entre los hinchas de River: "<< '\n';
    cout << '\n';
    printNombresMasComunes( 5, "River", inscriptos);
    cout << '\n';
    cout << "5) listado de equipos por socios con promedio de edades y edades maxima y minima de cada club" << '\n';
    cout << '\n';
    ListadoDeSociosYPromDeEdad(inscriptos);
}

// Run program: Ctrl + F5 or Debug > Start Without Debugging menu
// Debug program: F5 or Debug > Start Debugging menu
